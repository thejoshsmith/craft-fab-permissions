<?php
/**
 * Control Panel Permissions plugin for Craft CMS 3.x
 *
 * A plugin that allows admins to set tab and field restrictions for particular user groups in the system. For example, an admin could create a tabbed section that only they could see when creating entries.
 *
 * @link      https://joshsmith.dev
 * @copyright Copyright (c) 2019 Josh Smith
 */

/**
 * Control Panel Permissions en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('control-panel-permissions', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Josh Smith
 * @package   ControlPanelPermissions
 * @since     1.0.0
 */
return [
    'Control Panel Permissions plugin loaded' => 'Control Panel Permissions plugin loaded',
];
